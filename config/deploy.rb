# config valid only for current version of Capistrano
lock "3.9.1"

set :application, "myapp"
set :repo_url, "https://DialBird@bitbucket.org/DialBird/myapp.git"
set :user, 'keisuke'
set :ssh_options,
  keys: '~/.ssh/keisuke_aws.pem',
  user: fetch(:user)

# Default branch is :master
# set :branch, 'master'

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, "/data/#{fetch(:application)}"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true


# Default value for linked_dirs is []
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system'

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5

set :rbenv_type, :user
set :rbenv_ruby, '2.4.0'
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
append :rbenv_map_bins, 'puma', 'pumactl'
set :bundle_flags, "--deployment --without development test"
set :puma_tag, fetch(:application)
set :puma_conf, "#{shared_path}/config/puma.rb"

namespace :deploy do
  desc 'Make sure local git is in sync with remote.'
  task :check_revision do
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/master`
        puts "WARNING: HEAD is not the same as origin/master"
        puts "Run `git push` to sync changes."
        exit
      end
    end
  end

  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      before 'deploy:restart', 'puma:start'
      invoke 'deploy'
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5  do
      invoke 'puma:restart'
    end
  end

  desc 'Upload important files'
  task :upload

  before :starting, :check_revision
  before :starting, :upload
  after  :finishing, :compile_assets
  after  :finishing, :cleanup
end
